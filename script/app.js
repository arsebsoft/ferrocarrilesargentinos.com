var contadorEstacionesOperativas = 0;
var contadorEstacionesCerradas = 0;
var porcentajeEstacionesOperativas = 0;
var porcentajeEstacionesCerradas = 0;
var contadorLineasOperativas = 0;
var contadorLineasCerradas = 0;
var porcentajeLineasOperativas = 0;
var porcentajeLineasCerradas = 0;
var contadorTotalEstaciones = 0;
var contadorTotalLineas = 0;
var arrayNombres = [];
var arrayEstaciones = [];
var contador = 1;
var boolRamalesLoaded = false;
var boolEstacionesLoaded = false;
var boolMataderosBovinosLoaded = false;
var boolCanterasLoaded = false;
var boolAcopiosLoaded = false;
var boolElevadoresLoaded = false;
var boolApiculturaLoaded = false;
var boolCuencasLecherasLoaded = false;
var map = null;
var mySpinner = null;
var myChecker = null;
var contador;
var arrayEstacionesCarteles = [];
var thresholdEstaciones = 50;
var sidebar;
var capaBing, capaOSM, capaWatercolor, capaGray, capaTopo, capaTransporte, capaDeportes, capaMineria, capaActual, capaTerceros, indiceCapa, misRamales, misApiculturas, misCuencasLecheras, misMataderosBovinos, misCanteras, misEstaciones, misAcopios, misElevadores, topoApicultura, topoEstaciones, topoCuencasLecheras, topoElevadores, topoAcopios, topoRamales, topoCanteras, topoMataderosBovinos, tileApicultura, tileEstaciones, tileCuencasLecheras, tileAcopios, tileElevadores, tileMataderosBovinos, tileMinas, tileRamalesBg, tileRamalesFg;
var maxZoomAllowed = 18;
var minZoomAllowed = 2;

function ActualizarURL (latitud, longitud, altura) {

	window.history.replaceState("", "", "index.php?lat=" + latitud + "&lng=" + longitud + "&a=" + altura + "&capa=" + indiceCapa);
}

function ObtenerCoordenadasURL () {
	// Esta función analiza la URL y extrae de ella las coordenadas necesarias para inicializar la página o ir a un marcador.
	// Devuelve un objeto que contiene los miembros lat, lng, a y capa.
	var url = location.search;
	if (url == "") {
		url = "?lat=-34.574&lng=-58.458&a=14&capa=1&capaTerceros=0";
	}
	var query = url.substr(1);
	var result = {};
	query.split("&").forEach(function(part) {
		var item = part.split("=");
		result[item[0]] = decodeURIComponent(item[1]);
	});
	if (typeof result.lat !== 'undefined' && typeof result.lng !== 'undefined') {
		return result;
	}
	else {
		result.lat = -34.574;
		result.lng = -58.458;
		result.a = 14;
		result.capa = 1;

		return result;
	}
}

function CambiarPosicion (lat, lng, zoom) {
	map.spin(true, {lines: 10, length: 40});
	map.setView([lat, lng], zoom, {
		noMoveStart: true,
		animate: false
	});
	map.spin(false);
}

function RandomInteger (maxNumber) {
	return Math.floor(Math.random() * Math.floor(maxNumber));
}

function RandomColor () {
	var letters = '0123456789ABCDEF';
	var color = '#';
	for (var i = 0; i < 6; i++) {
		color += letters[Math.floor(Math.random() * 16)];
	}
	return color;
}

function checkEndLoading() {
	if (boolEstacionesLoaded && boolRamalesLoaded) {
		map.getPane("paneRamales").style.zIndex = 300;
		map.getPane("paneEstaciones").style.zIndex = 400;
		map.getPane("paneTerceros").style.zIndex = 200;
		map.getPane('paneTerceros').style.pointerEvents = 'none';
		var latlngh = ObtenerCoordenadasURL();
		CambiarPosicion (latlngh.lat,latlngh.lng,latlngh.a);
		map.spin(false);
		clearTimeout(myChecker);
	}
}

function CambiarCapaBase (myIndex) {
	$("img[name='imagenCapa']").removeClass("capaActiva");
	map.spin(true, {lines: 10, length: 40});

	var latlng = map.getCenter();
	var altura = map.getZoom();
	var nuevoFondo;

	switch (Number(myIndex)) {
		case 1:
			nuevoFondo = capaBing;
			indiceCapa = 1;
			$("#capaBing").addClass("capaActiva");
			break;
		case 2:
			nuevoFondo = capaOSM;
			indiceCapa = 2;
			$("#capaOSM").addClass("capaActiva");
			break;
		case 3:
			nuevoFondo = capaWatercolor;
			indiceCapa = 3;
			$("#capaWatercolor").addClass("capaActiva");
			break;
		case 4:
			nuevoFondo = capaGray;
			indiceCapa = 4;
			$("#capaGray").addClass("capaActiva");
			break;
		case 5:
			nuevoFondo = capaTopo;
			indiceCapa = 5;
			$("#capaTopo").addClass("capaActiva");
			break;
		case 6:
			nuevoFondo = capaTransporte;
			indiceCapa = 6;
			$("#capaTransporte").addClass("capaActiva");
			break;
		case 7:
			nuevoFondo = capaDeportes;
			indiceCapa = 7;
			$("#capaDeportes").addClass("capaActiva");
			break;
		default:
			nuevoFondo = capaBing;
			$("#capaBing").addClass("capaActiva");
			indiceCapa = 1;
	}

	ActualizarURL(latlng.lat.toFixed(6), latlng.lng.toFixed(6), altura);

	if (capaActual != null) {
		capaActual.remove();
	}
	capaActual = nuevoFondo;
	capaActual.addTo(map);
	map.spin(false);
}

function ToggleCapasPropias (myIndex) {
	map.spin(true, {lines: 10, length: 40});

	switch (Number(myIndex)) {
		case 1: // Estaciones
			if (boolEstacionesLoaded) {
				RemoverEstaciones();
				$("#capaEstaciones").removeClass("capaActiva");
			}
			else {
				CargarEstaciones();
				$("#capaEstaciones").addClass("capaActiva");
			}
			break;
		case 2: // Ramales
			if (boolRamalesLoaded) {
				RemoverRamales();
				$("#capaRamales").removeClass("capaActiva");
			}
			else {
				CargarRamales();
				$("#capaRamales").addClass("capaActiva");
			}
			break;
	}
	map.spin(false);
}

function ToggleCapasTerceros (myIndex) {
	map.spin(true, {lines: 10, length: 40});

	switch (Number(myIndex)) {
		case 1: // Canteras
			if (boolCanterasLoaded) {
				RemoverCanteras();
				$("#capaMineria").removeClass("capaActiva");
			}
			else {
				CargarCanteras();
				$("#capaMineria").addClass("capaActiva");
			}
			break;
		case 2: // Mataderos bovinos
			if (boolMataderosBovinosLoaded) {
				RemoverMataderosBovinos();
				$("#capaMataderosBovinos").removeClass("capaActiva");
			}
			else {
				CargarMataderosBovinos();
				$("#capaMataderosBovinos").addClass("capaActiva");
			}
			break;
		case 3: // Acopios
			if (boolAcopiosLoaded) {
				RemoverAcopios();
				$("#capaAcopio").removeClass("capaActiva");
			}
			else {
				CargarAcopios();
				$("#capaAcopio").addClass("capaActiva");
			}
			break;
		case 4: // Elevadores de granos
			if (boolElevadoresLoaded) {
				RemoverElevadores();
				$("#capaElevadores").removeClass("capaActiva");
			}
			else {
				CargarElevadores();
				$("#capaElevadores").addClass("capaActiva");
			}
			break;
		case 5: // Cuencas lecheras
			if (boolCuencasLecherasLoaded) {
				RemoverCuencasLecheras();
				$("#capaLechera").removeClass("capaActiva");
			}
			else {
				CargarCuencasLecheras();
				$("#capaLechera").addClass("capaActiva");
			}
			break;
		case 6: // Apicultura
			if (boolApiculturaLoaded) {
				RemoverApicultura();
				$("#capaApicultura").removeClass("capaActiva");
			}
			else {
				CargarApicultura();
				$("#capaApicultura").addClass("capaActiva");
			}
			break;
	}

	map.spin(false);
}

function ColorRamalesBg (x) {
	switch (x) {
		case 1:
			retValue = "#FFFFFF"; // Operativo
			break;
		case 2:
			retValue = "#FFFFFF"; // Precaución
			break;
		case 3:
			retValue = "#FFFFFF"; // Clausurado
			break;
		case 4:
			retValue = "#808080"; // Abandonado
			break;
		case 5:
			retValue = "#E3E319"; // Intransitable
			break;
		case 6:
			retValue = "#000000"; // Levantado
			break;
		case 7:
			retValue = "#000000"; // Proyectado
			break;
		case 8:
			retValue = "#808080"; // Trazado dudoso
			break;
		default:
			estadoRamal = "#808080"; // Desconocido
	}
	return retValue;
};

function ColorRamalesFg (x) {
	switch (x) {
		case 1:
			retValue = "#00801D"; // Operativo
			break;
		case 2:
			retValue = "#FFDD00"; // Precaución
			break;
		case 3:
			retValue = "#FF5500"; // Clausurado
			break;
		case 4:
			retValue = "#FF0000"; // Abandonado
			break;
		case 5:
			retValue = "#FF0000"; // Intransitable
			break;
		case 6:
			retValue = "#FF0000"; // Levantado
			break;
		case 7:
			retValue = "#1920E3"; // Proyectado
			break;
		case 8:
			retValue = "#E37190"; // Trazado dudoso
			break;
		default:
			estadoRamal = "#A5A5A5"; // Desconocido
	}
	return retValue;
};

function ColorRamales (x) {
	switch (x) {
		case 1:
			retValue = "#68D268"; // Operativo
			break;
		case 2:
			retValue = "#C3D268"; // Precaución
			break;
		case 3:
			retValue = "#D2B868"; // Clausurado
			break;
		case 4:
			retValue = "#D29268"; // Abandonado
			break;
		case 5:
			retValue = "#D2686C"; // Intransitable
			break;
		case 6:
			retValue = "#D26894"; // Levantado
			break;
		case 7:
			retValue = "#6A68D2"; // Proyectado
			break;
		case 8:
			retValue = "#939393"; // Trazado dudoso
			break;
		default:
			estadoRamal = "#555555"; // Desconocido
	}
	return retValue;
};

function ParsearRamalesFg (myData) {
	tileRamalesFg = L.geoJson(myData, {
		style: function (feature) {
			return {
				"color": ColorRamalesFg (feature.properties.cod_status),
				"opacity": 1,
				"weight": 4,
				"lineCap": "butt",
				"dashArray": "12"
			}
		},
		onEachFeature: function (feature, layer) {
			var estadoRamal = "";
			var nombreRamal = "s/d";
			var trochaRamal = "";
			if (feature.properties.fa85 != "" && feature.properties.fa85 != null) {
				nombreRamal = "sin asignación";
				if (feature.properties.fcgmb == 1) {
					nombreRamal = "Ferrocarril General Belgrano";
				}
				if (feature.properties.fced == 1) {
					nombreRamal = "Ferrocarriles y Elevadores Depietri";
				}
				if (feature.properties.fcgbm == 1) {
					nombreRamal = "Ferrocarril Bartolomé Mitre";
				}
				if (feature.properties.fcdfs == 1) {
					nombreRamal = "Ferrocarril Domingo Faustino Sarmiento";
				}
				if (feature.properties.fcgr == 1) {
					nombreRamal = "Ferrocarril General Roca";
				}
				if (feature.properties.fcgsm == 1) {
					nombreRamal = "Ferrocarril General San Martín";
				}
				if (feature.properties.fcgu == 1) {
					nombreRamal = "Ferrocarril General Urquiza";
				}
				if (feature.properties.agp == 1) {
					nombreRamal = "Administración General de Puertos";
				}
				nombreRamal = nombreRamal + " - " + feature.properties.fa85;
			}
			else {
				nombreRamal = "sin asignación";
				if (feature.properties.particular == 1) {
					nombreRamal = "Desvío particular";
				}
				if (feature.properties.fced == 1) {
					nombreRamal = "Ferrocarriles y Elevadores Depietri";
				}
				if (feature.properties.sin_asign == 1) {
					nombreRamal = "Sin asignar";
				}
				if (feature.properties.es_empalme != "" && feature.properties.es_empalme != null) {
					nombreRamal = "Empalme " + feature.properties.es_empalme;
				}
				if (feature.properties.es_interca != "" && feature.properties.es_interca != null) {
					nombreRamal = "Intercambio " + feature.properties.es_interca;
				}
				if (feature.properties.otro == 1) {
					nombreRamal = feature.properties.otro_ferro;
				}
			}

			if (feature.properties.decauville == 1) {
				trochaRamal = "Decauville (menor a 600 mm)";
			}
			else if (feature.properties.angosta == 1) {
				trochaRamal = "angosta (600 mm)";
			}
			else if (feature.properties.métrica == 1) {
				trochaRamal = "métrica (1000 mm)";
			}
			else if (feature.properties.estándar == 1) {
				trochaRamal = "estándar (1435 mm)";
			}
			else if (feature.properties.ancha == 1) {
				trochaRamal = "ancha (1676 mm)";
			}
			else {
				trochaRamal = "sin datos";
			}

			contadorTotalLineas++;
			switch (feature.properties.cod_status) {
				case 0:
					estadoRamal = "Desconocido";
					break;
				case 1:
					estadoRamal = "Operativo";
					break;
				case 2:
					estadoRamal = "A precaución";
					break;
				case 3:
					estadoRamal = "Clausurado";
					break;
				case 4:
					estadoRamal = "Abandonado";
					break;
				case 5:
					estadoRamal = "Intransitable";
					break;
				case 6:
					estadoRamal = "Levantado";
					break;
				case 7:
					estadoRamal = "Proyectado";
					break;
				case 8:
					estadoRamal = "Trazado dudoso";
					break;
				default:
					estadoRamal = "sin datos";
			}

			layer.on('mouseover', function (e) {
				layer.bindTooltip("<b>Ramal:</b> " + nombreRamal + "<br><b>Trocha:</b> " + trochaRamal + "<br><b>Estado:</b> " + estadoRamal, {sticky: true, opacity: 0.85}).openTooltip();
			});

		},
		pane: "paneRamales"
	}).addTo(map);
}

function ParsearRamales (myData) {
	var ramales = L.geoJson(myData, {
		style: function (feature) {
			return {
				"color": ColorRamales (feature.properties.cod_status),
				"opacity": 1,
				"weight": 4,
				"lineCap": "butt"
			}
		},
		onEachFeature: function (feature, layer) {
			var estadoRamal = "";
			var nombreRamal = "s/d";
			var trochaRamal = "";
			if (feature.properties.fa85 != "" && feature.properties.fa85 != null) {
				nombreRamal = "sin asignación";
				if (feature.properties.fcgmb == 1) {
					nombreRamal = "Ferrocarril General Belgrano";
				}
				if (feature.properties.fced == 1) {
					nombreRamal = "Ferrocarriles y Elevadores Depietri";
				}
				if (feature.properties.fcgbm == 1) {
					nombreRamal = "Ferrocarril Bartolomé Mitre";
				}
				if (feature.properties.fcdfs == 1) {
					nombreRamal = "Ferrocarril Domingo Faustino Sarmiento";
				}
				if (feature.properties.fcgr == 1) {
					nombreRamal = "Ferrocarril General Roca";
				}
				if (feature.properties.fcgsm == 1) {
					nombreRamal = "Ferrocarril General San Martín";
				}
				if (feature.properties.fcgu == 1) {
					nombreRamal = "Ferrocarril General Urquiza";
				}
				if (feature.properties.agp == 1) {
					nombreRamal = "Administración General de Puertos";
				}
				nombreRamal = nombreRamal + " - " + feature.properties.fa85;
			}
			else {
				nombreRamal = "sin asignación";
				if (feature.properties.particular == 1) {
					nombreRamal = "Desvío particular";
				}
				if (feature.properties.fced == 1) {
					nombreRamal = "Ferrocarriles y Elevadores Depietri";
				}
				if (feature.properties.sin_asign == 1) {
					nombreRamal = "Sin asignar";
				}
				if (feature.properties.es_empalme != "" && feature.properties.es_empalme != null) {
					nombreRamal = "Empalme " + feature.properties.es_empalme;
				}
				if (feature.properties.es_interca != "" && feature.properties.es_interca != null) {
					nombreRamal = "Intercambio " + feature.properties.es_interca;
				}
				if (feature.properties.otro == 1) {
					nombreRamal = feature.properties.otro_ferro;
				}
			}

			if (feature.properties.decauville == 1) {
				trochaRamal = "Decauville (menor a 600 mm)";
			}
			else if (feature.properties.angosta == 1) {
				trochaRamal = "angosta (600 mm)";
			}
			else if (feature.properties.métrica == 1) {
				trochaRamal = "métrica (1000 mm)";
			}
			else if (feature.properties.estándar == 1) {
				trochaRamal = "estándar (1435 mm)";
			}
			else if (feature.properties.ancha == 1) {
				trochaRamal = "ancha (1676 mm)";
			}
			else {
				trochaRamal = "sin datos";
			}

			contadorTotalLineas++;
			switch (feature.properties.cod_status) {
				case 0:
					estadoRamal = "Desconocido";
					break;
				case 1:
					estadoRamal = "Operativo";
					break;
				case 2:
					estadoRamal = "A precaución";
					break;
				case 3:
					estadoRamal = "Clausurado";
					break;
				case 4:
					estadoRamal = "Abandonado";
					break;
				case 5:
					estadoRamal = "Intransitable";
					break;
				case 6:
					estadoRamal = "Levantado";
					break;
				case 7:
					estadoRamal = "Proyectado";
					break;
				case 8:
					estadoRamal = "Trazado dudoso";
					break;
				default:
					estadoRamal = "sin datos";
			}

			layer.on('mouseover', function (e) {
				layer.bindTooltip("<b>Ramal:</b> " + nombreRamal + "<br><b>Trocha:</b> " + trochaRamal + "<br><b>Estado:</b> " + estadoRamal, {sticky: true, opacity: 0.85}).openTooltip();
			});

		},
		pane: "paneRamales"
	}).addTo(map);
}

function ParsearRamalesBg (myData) {
	tileRamalesBg = L.geoJson(myData, {
		style: function (feature) {
			return {
				"color": ColorRamalesBg (feature.properties.cod_status),
				"opacity": 1,
				"weight": 4,
				"lineCap": "butt"
			}
		},
		onEachFeature: function (feature, layer) {
			var estadoRamal = "";
			var nombreRamal = "s/d";
			var trochaRamal = "";
			if (feature.properties.fa85 != "" && feature.properties.fa85 != null) {
				nombreRamal = "sin asignación";
				if (feature.properties.fcgmb == 1) {
					nombreRamal = "Ferrocarril General Belgrano";
				}
				if (feature.properties.fced == 1) {
					nombreRamal = "Ferrocarriles y Elevadores Depietri";
				}
				if (feature.properties.fcgbm == 1) {
					nombreRamal = "Ferrocarril Bartolomé Mitre";
				}
				if (feature.properties.fcdfs == 1) {
					nombreRamal = "Ferrocarril Domingo Faustino Sarmiento";
				}
				if (feature.properties.fcgr == 1) {
					nombreRamal = "Ferrocarril General Roca";
				}
				if (feature.properties.fcgsm == 1) {
					nombreRamal = "Ferrocarril General San Martín";
				}
				if (feature.properties.fcgu == 1) {
					nombreRamal = "Ferrocarril General Urquiza";
				}
				if (feature.properties.agp == 1) {
					nombreRamal = "Administración General de Puertos";
				}
				nombreRamal = nombreRamal + " - " + feature.properties.fa85;
			}
			else {
				nombreRamal = "sin asignación";
				if (feature.properties.particular == 1) {
					nombreRamal = "Desvío particular";
				}
				if (feature.properties.fced == 1) {
					nombreRamal = "Ferrocarriles y Elevadores Depietri";
				}
				if (feature.properties.sin_asign == 1) {
					nombreRamal = "Sin asignar";
				}
				if (feature.properties.es_empalme != "" && feature.properties.es_empalme != null) {
					nombreRamal = "Empalme " + feature.properties.es_empalme;
				}
				if (feature.properties.es_interca != "" && feature.properties.es_interca != null) {
					nombreRamal = "Intercambio " + feature.properties.es_interca;
				}
				if (feature.properties.otro == 1) {
					nombreRamal = feature.properties.otro_ferro;
				}
			}

			if (feature.properties.decauville == 1) {
				trochaRamal = "Decauville (menor a 600 mm)";
			}
			else if (feature.properties.angosta == 1) {
				trochaRamal = "angosta (600 mm)";
			}
			else if (feature.properties.métrica == 1) {
				trochaRamal = "métrica (1000 mm)";
			}
			else if (feature.properties.estándar == 1) {
				trochaRamal = "estándar (1435 mm)";
			}
			else if (feature.properties.ancha == 1) {
				trochaRamal = "ancha (1676 mm)";
			}
			else {
				trochaRamal = "sin datos";
			}

			contadorTotalLineas++;
			switch (feature.properties.cod_status) {
				case 0:
					estadoRamal = "Desconocido";
					contadorLineasOperativas++;
					break;
				case 1:
					estadoRamal = "Operativo";
					contadorLineasOperativas++;
					break;
				case 2:
					estadoRamal = "A precaución";
					contadorLineasOperativas++;
					break;
				case 3:
					estadoRamal = "Clausurado";
					contadorLineasCerradas++;
					break;
				case 4:
					estadoRamal = "Abandonado";
					contadorLineasCerradas++;
					break;
				case 5:
					estadoRamal = "Intransitable";
					contadorLineasCerradas++;
					break;
				case 6:
					estadoRamal = "Levantado";
					contadorLineasCerradas++;
					break;
				case 7:
					estadoRamal = "Proyectado";
					contadorLineasCerradas++;
					break;
				case 8:
					estadoRamal = "Trazado dudoso";
					contadorLineasCerradas++;
					break;
				default:
					estadoRamal = "sin datos";
			}

			layer.on('mouseover', function (e) {
				layer.bindTooltip("<b>Ramal:</b> " + nombreRamal + "<br><b>Trocha:</b> " + trochaRamal + "<br><b>Estado:</b> " + estadoRamal, {sticky: true, opacity: 0.85}).openTooltip();
			});

		},
		pane: "paneRamales"
	}).addTo(map);

	$("#contadorLineasCerradas").text(contadorLineasCerradas + " de " + contadorTotalLineas);
	$("#contadorLineasOperativas").text(contadorLineasOperativas + " de " + contadorTotalLineas);

	porcentajeLineasCerradas = (contadorLineasCerradas / contadorTotalLineas) * 100
	porcentajeLineasOperativas = (contadorLineasOperativas / contadorTotalLineas) * 100

	$("#porcentajeLineasCerradas").attr("aria-valuenow", porcentajeLineasCerradas);
	$("#porcentajeLineasCerradas").attr("style", "width: " + porcentajeLineasCerradas + "%;");
	$("#porcentajeLineasOperativas").attr("aria-valuenow", porcentajeLineasOperativas);
	$("#porcentajeLineasOperativas").attr("style", "width: " + porcentajeLineasOperativas + "%;");
}

function ParsearEstaciones (dataEstaciones) {
	var estiloColorInterno = "";
	var estiloColorBorde = "";

	tileEstaciones = L.geoJson(dataEstaciones, {
		onEachFeature: function (feature, layer) {

			var estadoEstacion = "";
			var tipoEstacion = "";
			var elementoEstacion = {};
			var elementoNombre = {};

			switch (feature.properties.status) {
				case 0:
					estadoEstacion = "Sin datos";
					break;
				case 1:
					estadoEstacion = "Activa";
					break;
				case 2:
					estadoEstacion = "Sin personal";
					break;
				case 3:
					estadoEstacion = "Clausurada";
					break;
				case 4:
					estadoEstacion = "Abandonada";
					break;
				case 5:
					estadoEstacion = "Desafectada";
					break;
				case 6:
					estadoEstacion = "Demolida";
					break;
				default:
					estadoEstacion = "Sin datos";
			}

			switch (feature.properties.tipo) {
				case 1:
					tipoEstacion = "Parada";
					break;
				case 2:
					tipoEstacion = "Apeadero";
					break;
				case 3:
					tipoEstacion = "Estación de primera categoría";
					break;
				case 4:
					tipoEstacion = "Estación de segunda categoría";
					break;
				case 5:
					tipoEstacion = "Estación de combinación";
					break;
				case 6:
					tipoEstacion = "Terminal";
					break;
				case 7:
					tipoEstacion = "Cargas";
					break;
				default:
					tipoEstacion = "Sin datos";
			}

			// Creamos el nombre de la estación.

			layer.bindTooltip(feature.properties.nombre, {
				permanent: false,
				className: "nombresEstaciones"
			});

			elementoEstacion.latlng = L.latLng(feature.geometry.coordinates[1], feature.geometry.coordinates[0]);
			elementoEstacion.latitud = feature.geometry.coordinates[1];
			elementoEstacion.longitud = feature.geometry.coordinates[0];
			elementoEstacion.nombre = feature.properties.nombre;
			elementoEstacion.uuid = feature.properties.uuid;
			elementoEstacion.ferrocarril = feature.properties.ferrocarril;
			elementoEstacion.indice = contador;
			arrayEstaciones.push (elementoEstacion);
			elementoNombre.label = feature.properties.nombre + " (" + feature.properties.ferrocarril + ")";
			elementoNombre.value = contador;
			arrayNombres.push (elementoNombre);

			layer.on('click', function (e) {
				var myHTML = "<div name='popupEstacion' uuid='" + feature.properties.uuid + "'><b style='text-align: center;'>" + feature.properties.nombre + "</b><br>" + estadoEstacion + "<div name='detallesAdicionalesEstacion' uuid='" + feature.properties.uuid + "'><br>Ferrocarril: " + feature.properties.ferrocarril + "<br>Tipo: " + tipoEstacion;
				if (feature.properties.article) {
					myHTML = myHTML + "<hr><div><p><b>Patrocinan esta estación:</b></p><p><a href='#' target='_blank'>Tu nombre aquí</a></p></div>";
					myHTML = myHTML + "<hr><input type='button' name='detalleEstaciones' uuid='" + feature.properties.uuid + "' value='Ver artículo'></div>";
				}
				layer.bindPopup(myHTML);
			});

			contador++;

		},
		pointToLayer: function (feature, latlng) {
			switch (feature.properties.status) {
				case 1: // Activa
					estiloColorInterno = "#00C800";
					estiloColorBorde = "#FFFFFF";
					break;
				case 2: // Sin personal
					estiloColorInterno = "#C8C800";
					estiloColorBorde = "#000000";
					break;
				case 3: // Clausurada
					estiloColorInterno = "#D66300";
					estiloColorBorde = "#000000";
					break;
				case 4: // Abandonada
					estiloColorInterno = "#D66300";
					estiloColorBorde = "#646464";
					break;
				case 5: // Desafectada
					estiloColorInterno = "#C80000";
					estiloColorBorde = "#FFFFFF";
					break;
				case 6: // Demolida
					estiloColorInterno = "#FFFFFF";
					estiloColorBorde = "#C80000";
					break;
				default:
					estiloColorInterno = "#808080";
					estiloColorBorde = "#101010";
			}

			var geojsonMarkerOptions = {
				zIndexOffset: 800,
				radius: 6,
				fillColor: estiloColorInterno,
				color: estiloColorBorde,
				weight: 2,
				opacity: 1,
				fillOpacity: 1
			};
			return L.circleMarker(latlng, geojsonMarkerOptions);
		},
		pane: "paneEstaciones"
	}).addTo(map);
}

function ParsearMataderosBovinos (myData) {

	tileMataderosBovinos = L.geoJson(myData, {
		onEachFeature: function (feature, layer) {

			var actividad = feature.properties.actividad;
			var exportador = feature.properties.bov_exp;
			var ciclo = feature.properties.bov_ciclo;

			if (exportador == "" || exportador == null) {
				exportador = "no exportador";
			}
			else {
				exportador = "exportador";
			}

			if (ciclo == "" || ciclo == null) {
				ciclo = "s/d";
			}

			var nombre = actividad + ", " + exportador + ", " + ciclo;

			layer.bindTooltip(nombre, {
				permanent: false,
				className: "nombresEstaciones"
			});

		},
		pointToLayer: function (feature, latlng) {
			var myIcon = L.icon({
				iconUrl: 'images/icons/bovinos.png',
				iconSize: [24, 24],
				iconAnchor: [12, 12],
				popupAnchor: [-3, -76]
			});
			var markerOptions = {
				zIndexOffset: 800,
				icon: myIcon
			};
			return L.marker(latlng, markerOptions);
		},
		pane: "paneTerceros"
	}).addTo(map);
}

function ParsearElevadores (myData) {

	tileElevadores = L.geoJson(myData, {
		onEachFeature: function (feature, layer) {

			layer.bindTooltip(feature.properties.nombre, {
				permanent: false,
				className: "nombresEstaciones"
			});

		},
		pointToLayer: function (feature, latlng) {
			var myIcon = L.icon({
				iconUrl: 'images/icons/elevadores.png',
				iconSize: [24, 24],
				iconAnchor: [12, 12],
				popupAnchor: [-3, -76]
			});
			var markerOptions = {
				zIndexOffset: 800,
				icon: myIcon
			};
			return L.marker(latlng, markerOptions);
		},
		pane: "paneTerceros"
	}).addTo(map);
}

function ParsearAcopios (param) {
	tileAcopios = L.geoJSON(param, {
		style: function (feature) {
			myColor = RandomColor();
			return {
			"zIndexOffset": 400,
			"color": 'black',
			"opacity": 1,
			"fill": true,
			"fillColor": myColor,
			"fillOpacity": 0.4
			};
		},
		pane: "paneTerceros"
	}).bindTooltip(function (layer) {
		var departamento = layer.feature.properties.departamen;
		var capacidad = layer.feature.properties.cap;
		var minCap = layer.feature.properties.cantidad_d * 1000;
		var maxCap = layer.feature.properties.camtidad_d * 1000;
		return departamento + ", cap. " + minCap + " a " + maxCap + " t";
	}, {permanent: false, className: "nombresDepartamentos", sticky: true}).addTo(map);
}

function ParsearCuencasLecheras (param) {
	tileCuencasLecheras = L.geoJSON(param, {
		style: function (feature) {
			myColor = RandomColor();
			return {
			"zIndexOffset": 400,
			"color": 'black',
			"opacity": 1,
			"fill": true,
			"fillColor": myColor,
			"fillOpacity": 0.4
			};
		},
		pane: "paneTerceros"
	}).bindTooltip(function (layer) {
		var cuenca = layer.feature.properties.cuenca;
		var link = layer.feature.properties.link;
		return cuenca + ", link: " + link.toFixed(0);
	}, {permanent: false, className: "nombresDepartamentos", sticky: true}).addTo(map);
}

function ParsearApicultura (param) {
	tileApicultura = L.geoJSON(param, {
		style: function (feature) {
			myColor = RandomColor();
			return {
			"zIndexOffset": 400,
			"color": 'black',
			"opacity": 1,
			"fill": true,
			"fillColor": myColor,
			"fillOpacity": 0.4
			};
		},
		pane: "paneTerceros"
	}).bindTooltip(function (layer) {
		var partido = layer.feature.properties.partido;
		var apicultores = layer.feature.properties.cantidad_a;
		var cantidad = layer.feature.properties.cantidad_c;
		return partido + "; apicultores: " + apicultores + ", colmenas: " + cantidad;
	}, {permanent: false, className: "nombresDepartamentos", sticky: true}).addTo(map);
}

function ParsearCanteras (param) {

	// nombre: extraer de fna, eliminar valores null, vacíos o con "-1".

	var myStyle = {
		zIndexOffset: 800,
		color: 'black',
		opacity: 1,
		fill: true,
		fillColor: 'red',
		fillOpacity: 0.4
	};

	tileMinas = L.geoJSON(param, myStyle).bindTooltip(function (layer) {
		var elementoNombre = layer.feature.properties.fna;

		if (elementoNombre == null || elementoNombre == "" || elementoNombre == "-1") {
			elementoNombre = "Cantera";
		}
		return elementoNombre;
	}, {permanent: false, className: "nombresEstaciones"}).addTo(map);

}

function ProbeAndToggleLabels() {
		var counter = 0;

		for (var f in tileEstaciones._layers) {
			var feature = tileEstaciones._layers[f];
			if(map.getBounds().contains(feature._latlng)) {
				counter++;
			}
		};

		if (counter < thresholdEstaciones) {
			for (var f in tileEstaciones._layers) {
				var feature = tileEstaciones._layers[f];
				var tooltip = feature._tooltip;
				if(map.getBounds().contains(feature._latlng)) {
					feature.unbindTooltip();
					feature.bindTooltip(tooltip, {
						permanent: true
					});
				}
			};
		}
		else {
			map.eachLayer(function(layer) {
				if (layer.getTooltip()) {
					var tooltip = layer.getTooltip();
					layer.unbindTooltip().bindTooltip(tooltip, {
						permanent: false
					});
				}
			});
		}

		var mylatlng = map.getCenter();
		var myHeight = map.getZoom();
		ActualizarURL (mylatlng.lat.toFixed(6), mylatlng.lng.toFixed(6), myHeight);
}

function CargarEstaciones () {
	misEstaciones = L.geoJson(null, {
		style: {
			radius: 6,
			fillColor: 'green',
			color: 'black',
			weight: 2,
			opacity: 1,
			fillOpacity: 1
		}
	});

	topoEstaciones = omnivore.topojson("layers/estaciones.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misEstaciones);

	topoEstaciones.on('ready', function() {
		var dataEstaciones = topoEstaciones.toGeoJSON();
		ParsearEstaciones (dataEstaciones);
		boolEstacionesLoaded = true;
	});
}

function RemoverEstaciones () {
	tileEstaciones.remove();
	boolEstacionesLoaded = false;
}

function CargarCanteras () {
	misCanteras = L.geoJson(null);

	topoCanteras = omnivore.topojson("layers/canteras.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misCanteras);

	topoCanteras.on('ready', function() {
		var dataCanteras = topoCanteras.toGeoJSON();
		ParsearCanteras (dataCanteras);
		boolCanterasLoaded = true;
	});
}

function RemoverCanteras () {
	tileMinas.remove();
	boolCanterasLoaded = false;
}

function CargarCuencasLecheras () {
	misCuencasLecheras = L.geoJson(null);

	topoCuencasLecheras = omnivore.topojson("layers/cuencas-lecheras.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misCuencasLecheras);

	topoCuencasLecheras.on('ready', function() {
		var dataCuencasLecheras = topoCuencasLecheras.toGeoJSON();
		ParsearCuencasLecheras (dataCuencasLecheras);
		boolCuencasLecherasLoaded = true;
	});
}

function RemoverCuencasLecheras () {
	tileCuencasLecheras.remove();
	boolCuencasLecherasLoaded = false;
}

function CargarApicultura () {
	misApiculturas = L.geoJson(null);

	topoApicultura = omnivore.topojson("layers/apicultura.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misApiculturas);

	topoApicultura.on('ready', function() {
		var dataApicultura = topoApicultura.toGeoJSON();
		ParsearApicultura (dataApicultura);
		boolApiculturaLoaded = true;
	});
}

function RemoverApicultura () {
	tileApicultura.remove();
	boolApiculturaLoaded = false;
}

function CargarAcopios () {
	console.log ("Probando acopios");
	misAcopios = L.geoJson(null);

	topoAcopios = omnivore.topojson("layers/acopios.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misAcopios);

	topoAcopios.on('ready', function() {
		var dataAcopio = topoAcopios.toGeoJSON();
		ParsearAcopios (dataAcopio);
		boolAcopiosLoaded = true;
	});
}

function RemoverAcopios () {
	tileAcopios.remove();
	boolAcopiosLoaded = false;
}

function CargarMataderosBovinos () {
	misMataderosBovinos = L.geoJson();

	topoMataderosBovinos = omnivore.topojson("layers/mataderos-frigo-bovinos.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misMataderosBovinos);

	topoMataderosBovinos.on('ready', function() {
		var dataMataderosBovinos = topoMataderosBovinos.toGeoJSON();
		ParsearMataderosBovinos (dataMataderosBovinos);
		boolMataderosBovinosLoaded = true;
	});
}

function RemoverMataderosBovinos () {
	tileMataderosBovinos.remove();
	boolMataderosBovinosLoaded = false;
}

function CargarElevadores () {
	misElevadores = L.geoJson();

	topoElevadores = omnivore.topojson("layers/elevadores-granos.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misElevadores);

	topoElevadores.on('ready', function() {
		var dataElevadores = topoElevadores.toGeoJSON();
		ParsearElevadores (dataElevadores);
		boolElevadoresLoaded = true;
	});
}

function RemoverElevadores () {
	tileElevadores.remove();
	boolElevadoresLoaded = false;
}

function CargarRamales () {
	misRamales = L.geoJson(null, {
		style: {
			color: 'yellow'
		}
	});

	topoRamales = omnivore.topojson("layers/ramales.json" + "?a=" + RandomInteger() + "&b=" + RandomInteger() + "&c=" + RandomInteger(), null, misRamales);

	topoRamales.on('ready', function() {
		var highlight;
		var dataRamales = topoRamales.toGeoJSON();
		ParsearRamalesBg(dataRamales);
		ParsearRamalesFg(dataRamales);
		boolRamalesLoaded = true;
	});
}


function RemoverRamales () {
	tileRamalesBg.remove();
	tileRamalesFg.remove();
	boolRamalesLoaded = false;
}

function InitiateLeafletMap() {
	var latlngh = ObtenerCoordenadasURL();
	indiceCapa = latlngh.capa;

	var mapOptions = {
		zoomControl: false,
		center: [latlngh.lat, latlngh.lng],
		zoom: latlngh.a,
		maxZoom: maxZoomAllowed,
		minZoom: minZoomAllowed,
		sleep: false,
		sleepNote: false,
		visualClickEvents: 'click',
		preferCanvas: false
	};

	map = new L.map('map', mapOptions);
	map.createPane("paneRamales");
	map.createPane("paneEstaciones");
	map.createPane("paneTerceros");
	map.createPane("paneEtiquetas");
	sidebar = L.control.sidebar('sidebar').addTo(map);
	map.spin(true, {lines: 10, length: 40});

	map.on('zoomend', function () {
		ProbeAndToggleLabels();
	});

	map.on('moveend', function () {
		ProbeAndToggleLabels();
	});

	capaBing = L.tileLayer.bing("AgOU05hZ-Lbygf9z0zR1dgwDmBaBXspZ0ALXnrGXgWcwrFeplik_bVjnoDAY8CbX");

	capaOSM = L.tileLayer('https://{s}.tile.openstreetmap.fr/hot/{z}/{x}/{y}.png', {
		maxZoom: maxZoomAllowed,
		attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>, Tiles courtesy of <a href="http://hot.openstreetmap.org/" target="_blank">Humanitarian OpenStreetMap Team</a>'
	});

	capaWatercolor = L.tileLayer('//stamen-tiles-{s}.a.ssl.fastly.net/watercolor/{z}/{x}/{y}.png', {
		attribution: 'Map tiles by <a href="http://stamen.com">Stamen Design</a>, <a href="http://creativecommons.org/licenses/by/3.0">CC BY 3.0</a> &mdash; Map data &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
		subdomains: 'abcd',
		maxNativeZoom: maxZoomAllowed,
		maxZoom: maxZoomAllowed,
		ext: 'png'
	 });

	capaTransporte = L.tileLayer(	'https://{s}.tile.thunderforest.com/transport/{z}/{x}/{y}.png?apikey=06bb7eb0d1b34a96a492f3bf273e1d0c', {
	attribution: '&copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	maxZoom: maxZoomAllowed
	});

	capaDeportes = L.tileLayer(	'https://{s}.tile.thunderforest.com/outdoors/{z}/{x}/{y}.png?apikey=06bb7eb0d1b34a96a492f3bf273e1d0c', {
	attribution: '&copy; <a href="http://www.opencyclemap.org">OpenCycleMap</a>, &copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>',
	maxZoom: maxZoomAllowed
	});

	capaGray = L.tileLayer('//server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
	maxNativeZoom: maxZoomAllowed,
	maxZoom: maxZoomAllowed,
	});

	capaTopo = L.tileLayer('//server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}', {
	attribution: 'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community',
	maxZoom: maxZoomAllowed,
	maxNativeZoom: maxZoomAllowed,
	});

	CambiarCapaBase (indiceCapa);

// Otros controles
	var myMeasureRuler = L.control.scale({metric: true, imperial: false, maxWidth: 200, updateWhenIdle: true}).addTo(map);

	var customZoom = L.control.zoom({
		zoomInTitle: "Acercar",
		zoomOutTitle: "Alejar"
	}).addTo(map);

	var locateMe = L.control.locate({
		strings: {
			title: "Hallar mi ubicación"
		}
	}).addTo(map);

	var myRuler = L.control.ruler(
		{
			position: 'topleft',
			lengthUnit: {
				display: 'km',
				decimal: 3,
				factor: null,
				label: 'Distancia:'
			},
			angleUnit: {
				display: '&deg;',
				decimal: 2,
				factor: null,
				label: 'Rumbo (ángulo):'
			}
		}
	).addTo(map);

	L.control.coordinates(
		{
			position: "topright",
			decimals: 4,
			decimalSeperator: ",",
			labelTemplateLat:"Latitud: {y}",
			labelTemplateLng:"Longitud: {x}",
			enableUserInput: false,
			useDMS: true,
			useLatLngOrder: true
		}
	).addTo(map);

	// Carga de estaciones
	ToggleCapasPropias(1);
	// Carga de ramales
	ToggleCapasPropias(2);
}

$("#map").on("click", "input[name='detalleEstaciones']", ( function() {
	var uuid = $(this).attr("uuid");
	uuid = uuid.replace('{','');
	uuid = uuid.replace('}','');
	var myURL = "http://enciclopedia.ferrocarrilesargentinos.com/blog/" + uuid;
	window.open(myURL,'_blank');
	return false;
}));

$('#buscador').autocomplete ({
	minChars: 3,
	source: function (term, suggest) {
		term = $('#buscador').val().toLowerCase();
		var matches = [];
		var totalEstaciones = arrayNombres.length;
		for (i = 0; i < totalEstaciones; i++) {
			if (~arrayNombres[i].label.toLowerCase().indexOf(term)) {
				matches.push (arrayNombres[i]);
			}
		}
		suggest (matches);
	},
	search: function(event, ui) {
		setTimeout(() => {
			let w = $(this).autocomplete("widget").find("div"),
			re = new RegExp("("+this.value+")", "i");
			w.html((i, html) =>
				html.replace(re, "<span class='highlighted'>$1</span>")
			);
		},250);
	},
	select: function(event, ui) {
		$.notify("Localizando estación...", "info");
		event.preventDefault();
		$('#buscador').val('');
		CambiarPosicion (arrayEstaciones[ui.item.value - 1].latitud, arrayEstaciones[ui.item.value - 1].longitud, 16);
		sidebar.close();
		return false;
	}
});

$(document).ready(function() {

	$.notify.defaults (
		{
			// whether to hide the notification on click
			clickToHide: false,
			// whether to auto-hide the notification
			autoHide: true,
			// if autoHide, hide after milliseconds
			autoHideDelay: 5000,
			// show the arrow pointing at the element
			arrowShow: false,
			// default positions
			elementPosition: 'bottom left',
			globalPosition: 'top right',
			// default style
			style: 'bootstrap',
			// default class (string or [string])
			className: 'info',
			gap: 2
		}
	);

	myChecker = setInterval (checkEndLoading, 500);
	InitiateLeafletMap();
});

$('#capaBing').on('click', function () {
	CambiarCapaBase (1);
	sidebar.close();
	indiceCapa = 1;
});
$("#capaOSM").on('click', function () {
	CambiarCapaBase (2);
	sidebar.close();
	indiceCapa = 2;
});
$("#capaWatercolor").on('click', function () {
	CambiarCapaBase (3);
	sidebar.close();
	indiceCapa = 3;
});
$("#capaGray").on('click', function () {
	CambiarCapaBase (4);
	sidebar.close();
	indiceCapa = 4;
});
$("#capaTopo").on('click', function () {
	CambiarCapaBase (5);
	sidebar.close();
	indiceCapa = 5;
});
$("#capaTransporte").on('click', function () {
	CambiarCapaBase (6);
	sidebar.close();
	indiceCapa = 6;
});
$("#capaDeportes").on('click', function () {
	CambiarCapaBase (7);
	sidebar.close();
	indiceCapa = 7;
});

$("#capaEstaciones").on('click', function () {
	ToggleCapasPropias (1);
	sidebar.close();
});
$("#capaRamales").on('click', function () {
	ToggleCapasPropias (2);
	sidebar.close();
});
$("#capaMineria").on('click', function () {
	ToggleCapasTerceros (1);
	sidebar.close();
});
$("#capaMataderosBovinos").on('click', function () {
	ToggleCapasTerceros (2);
	sidebar.close();
});
$("#capaAcopio").on('click', function () {
	ToggleCapasTerceros (3);
	sidebar.close();
});
$("#capaElevadores").on('click', function () {
	ToggleCapasTerceros (4);
	sidebar.close();
});
$("#capaLechera").on('click', function () {
	ToggleCapasTerceros (5);
	sidebar.close();
});
$("#capaApicultura").on('click', function () {
	ToggleCapasTerceros (6);
	sidebar.close();
});
$("#botonbusqueda").on('click', function () {
	$("#buscador").delay(250).focus();
});