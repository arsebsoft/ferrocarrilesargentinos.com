<?php
/*
--------------------------------------------------------------------------------
Proxy para enviar correos
=========================

Version: 1.0

Author: Ariel S. Becker
Author"s mail: barroco@gmail.com

Description
===========

Este proxy se encarga de enviar mensajes de correo.
--------------------------------------------------------------------------------
*/

if (isset ($_POST["proxycall"])) {
	$proxycall = filter_var ($_POST["proxycall"], FILTER_SANITIZE_STRING);
	// Se busca qué función debe llamarse.
	switch ($proxycall) {
		case "enviarMensaje":
			$nombre = filter_var ($_POST["nombre"], FILTER_SANITIZE_STRING);
			$email = filter_var ($_POST["email"], FILTER_SANITIZE_STRING);
			$mensaje = filter_var ($_POST["mensaje"], FILTER_SANITIZE_STRING);

			$body = $nombre . " (" . $email . ") ha enviado el siguiente mensaje: <br><br>" . $mensaje;
			$to = "info@ferrocarrilesargentinos.com";
			$subject = "Mensaje desde Ferrocarriles Argentinos";
			$headers = "From: " . $nombre . " <" . $email . ">\r\n";
			$headers .= "To: barroco@gmail.com\r\n";
			$headers .= "Reply-To: " . $email ."\r\n";
			$headers .= "MIME-Version: 1.0\r\n";
			$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

			if (mail ($to, $subject, $body, $headers)) {
				echo "OK";
			}
			else {
				echo "error";
			}
			exit ();
			break;
	}
}
else {
	echo "No se puede llamar directamente esta función";
	die ();
}

?>