<?php
/*
--------------------------------------------------------------------------------
Proxy para trabajar con llamadas POST
=====================================

Version: 0.1

Author: Ariel S. Becker
Author"s mail: barroco@gmail.com

Description
===========

Este php es un proxy para manejar peticiones POST.
--------------------------------------------------------------------------------
*/
require_once "core.php";

try {
	if (isset($_POST["proxycall"])) {
		$proxycall = filter_var($_POST["proxycall"], FILTER_SANITIZE_STRING);

		// Se busca qué función debe llamarse.
		switch ($proxycall) {
			case "obtenerDatosEstaciones":
				$uuid = filter_var($_POST["uuid"], FILTER_SANITIZE_STRING);

				error_log ("UUID: " . $uuid);

				$objCore = new Core ();
				$objDatosEstacion = new stdClass ();
				$objDatosEstacion->datos = $objCore->ObtenerArrayEstacion ($uuid);
				$objDatosEstacion->foto = $objCore->ObtenerFotosEstacion ($uuid, 1);

				$retValue = json_encode ($objDatosEstacion);

				$objCore = null;
				echo $retValue;
				exit ();
				break;
		}
	}
	else {
		echo "No se puede llamar directamente esta función";
		die ();
	}
}
catch (exception $e) {
	error_log ("Error: " . $e);
}
?>