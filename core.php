<?php
/*
--------------------------------------------------------------------------------
Clase para manejar el core del sistema
======================================

Versión: 0.2

Autor: Ariel S. Becker
Email: barroco@gmail.com

Descripción
===========

Funciones core de Ferrocarriles Argentinos.
--------------------------------------------------------------------------------
*/

class Core {

	/*
	* ---------------------------------------------------------------------------
	* Métodos públicos
	* ---------------------------------------------------------------------------
	*/

	public function ObtenerArrayEstacion ($uuid) {
		try {
			$this->_initializeDBConn ();
			$sqlStatement = $this->sqlDBHandler->prepare ("SELECT * FROM Estaciones WHERE uuid = :uuid;");
			$sqlStatement->bindParam (':uuid', $uuid, PDO::PARAM_STR);
			$sqlStatement->execute ();
			$retValue = $sqlStatement->fetch(PDO::FETCH_OBJ);
		}
		catch (exception $e) {
			error_log("Error PDO: " . $e);
			$retValue = false;
		}
		$sqlStatement = null;
		$this->_destroyDBConn ();
		return $retValue;
	}
	public function ObtenerFotosEstacion ($uuid, $destacada) {
		try {
			$this->_initializeDBConn ();
			if ($destacada != null && $destacada == 1) {
				$sqlStatement = $this->sqlDBHandler->prepare ("SELECT archivo FROM FotosEstaciones WHERE uuid = :uuid AND destacada = 1;");
			}
			else {
				$sqlStatement = $this->sqlDBHandler->prepare ("SELECT archivo FROM FotosEstaciones WHERE uuid = :uuid;");
			}
			$sqlStatement->bindParam (':uuid', $uuid, PDO::PARAM_STR);
			$sqlStatement->execute ();
			if ($destacada == 1) {
				$retValue = $sqlStatement->fetchColumn();
			}
			else {
				$retValue = $sqlStatement->fetch(PDO::FETCH_OBJ);
			}

			if ($retValue == null) {
				$retValue = "nofoto.jpg";
			}
		}
		catch (exception $e) {
			error_log("Error PDO: " . $e);
			$retValue = false;
		}
		$sqlStatement = null;
		$this->_destroyDBConn ();
		return $retValue;
	}

	/*
	* ---------------------------------------------------------------------------
	* Métodos privados
	* ---------------------------------------------------------------------------
	*/

	private function _initializeDBConn () {
		try {
			$mysql_hostname = 'localhost';
			$mysql_username = 'ferrocar_user';
			$mysql_password = 'xzaqh548';
			$mysql_dbname = "ferrocar_test";
			$this->sqlDBHandler = new PDO("mysql:host=$mysql_hostname;dbname=$mysql_dbname;charset=utf8", $mysql_username, $mysql_password);
			$this->sqlDBHandler->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
		}
		catch (exception $e) {
			error_log("Error PDO: " . $e);
		}
	}

	private function _destroyDBConn () {
		$this->sqlDBHandler = null;
	}

}

?>