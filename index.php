<?php
	/*
	--------------------------------------------------------------------------------
	SIG Ferrocarriles Argentinos
	============================

	Versión: 0.6

	Autor: Ariel Sebastián Becker
	Email: barroco@gmail.com

	Descripción
	===========

	Mapa que muestra las capas y permite interactuar con ellas.
	--------------------------------------------------------------------------------
	*/

	//require_once "core.php"

	header("Cache-Control: no-cache, no-store, must-revalidate"); // HTTP 1.1.
	header("Pragma: no-cache"); // HTTP 1.0.
	header("Expires: 0"); // Proxies.

?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<title>SIG Ferrocarriles Argentinos</title>
		<meta name="description" content="Sistema de Información Geográfica de los Ferrocarriles Argentinos, versión 0.6">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="robots" content="all,follow">

		<!-- The following are some OpenGraph metatags; you can find them at https://wiki.whatwg.org/wiki/MetaExtensions -->

		<!-- Schema.org markup for Facebook and Open Graph -->
		<meta property="og:url" content="https://www.ferrocarrilesargentinos.com" />
		<meta property="og:type" content="website" />
		<meta property="og:title" content="SIG Ferrocarriles Argentinos" />
		<meta property="og:image" content="https://www.ferrocarrilesargentinos.com/images/social.png" />
		<meta property="og:image:secure_url" content="https://www.ferrocarrilesargentinos.com/images/social.png" />
		<meta property="og:image:type" content="image/png" />
		<meta property="og:image:width" content="400" />
		<meta property="og:image:height" content="400" />
		<meta property="og:description" content="Sistema de Información Geográfica de los Ferrocarriles Argentinos, versión 0.6" />
		<meta property="og:locale" content="es_ES" />
		<meta name="geo.country" content="ar">
		<meta name="microtip" content="1MdfB7Pn8revosFZsoighKGd3VZ5sgjtek">
		<meta name="contact" content="info@ferrocarrilesargentinos.com">

		<!-- Twitter Card data -->
		<meta name="twitter:card" content="summary" />
		<meta name="twitter:site" content="@ffcc_argentinos" />
		<meta name="twitter:title" content="SIG Ferrocarriles Argentinos" />
		<meta name="twitter:description" content="Sistema de Información Geográfica de los Ferrocarriles Argentinos, versión 0.6" />
		<meta name="twitter:creator" content="@ffcc_argentinos" />
		<meta name="twitter:image" content="https://www.ferrocarrilesargentinos.com/images/social.jpg" />

		<!-- Bootstrap and Font Awesome css-->
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
		<link rel="stylesheet" href="styles/bootstrap.min.css">
		<!-- Google fonts-->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Staatliches:300,400,700">
		<!-- Theme stylesheet-->
		<link rel="stylesheet" href="styles/style.default.css" id="theme-stylesheet">
		<link type="text/css" rel="stylesheet" href="styles/jquery-autocomplete.css">
		<link rel="stylesheet" href="styles/jquery-ui.css" id="theme-stylesheet">
		<link type="text/css" rel="stylesheet" href="styles/leaflet.css">
		<link type="text/css" rel="stylesheet" href="styles/leaflet-sidebar.css">
		<link type="text/css" rel="stylesheet" href="styles/Leaflet.Coordinates-0.1.3.css">
		<link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/gokertanrisever/leaflet-ruler/master/src/leaflet-ruler.css">
		<link type="text/css" rel="stylesheet" href="styles/notify.css">
		<link type="text/css" rel="stylesheet" href="styles/spin.css">
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol/dist/L.Control.Locate.min.css" />
		<link type="text/css" rel="stylesheet" href="styles/custom.css">

		<!-- JAVASCRIPT FILES -->
		<script src="script/jquery.js"></script>
		<script src="script/jquery-autocomplete.js"></script>
		<script src="script/bootstrap.js"></script>
		<script src="script/spin.js"></script>
		<script src="script/jquery-ui.js"></script>
		<script src="script/jquery.cookie.js"></script>
		<!--<script src="script/front.js"></script>-->
		<script src="script/leaflet.js"></script>
		<script src='//api.tiles.mapbox.com/mapbox.js/plugins/leaflet-omnivore/v0.3.1/leaflet-omnivore.min.js'></script>
		<script src="https://unpkg.com/leaflet.vectorgrid@latest/dist/Leaflet.VectorGrid.bundled.js"></script>
		<script src="script/leaflet-sidebar.js"></script>
		<script src="script/leaflet-bing-layer.js"></script>
		<script src="script/leaflet.spin.js"></script>
		<script src="script/Leaflet.Coordinates-0.1.3.min.js"></script>
		<script src="https://cdn.jsdelivr.net/npm/leaflet.locatecontrol/dist/L.Control.Locate.min.js" charset="utf-8"></script>
		<script src="https://cdn.rawgit.com/gokertanrisever/leaflet-ruler/master/src/leaflet-ruler.js"></script>
		<script src="script/notify.js"></script>
		<!-- For older browsers to sucessfully load the Bing tile layer -->
		<script src="https://cdn.polyfill.io/v2/polyfill.min.js?features=Promise"></script>
		<script>
			$( function() {
				$( "#panelCapas" ).accordion(
					{
						heightStyle: "content",
						collapsible: true
					}
				);
				$( "#changelogd" ).accordion(
					{
						heightStyle: "content",
						collapsible: true
					}
				);
			});
		</script>
		<!-- Favicon-->
		<link rel="shortcut icon" href="favicon.png">
		<!-- Tweaks for older IEs--><!--[if lt IE 9]>
				<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
				<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
	</head>
	<body>

<!-- Barra lateral -->
		<div id="sidebar" class="sidebar collapsed">
			<!-- Nav tabs -->
			<div class="sidebar-tabs">
				<ul role="tablist">
					<li title="Acerca del proyecto">
						<a href="#acerca" role="tab" target="_blank">
							<i class="fas fa-info-circle">
							</i>
						</a>
					</li>
					<li title="Leyendas y símbolos">
						<a href="#leyendas" role="tab">
							<i class="fas fa-map">
							</i>
						</a>
					</li>
					<li title="Buscador de estaciones">
						<a href="#buscar" role="tab" id="botonbusqueda">
							<i class="fas fa-search-location">
							</i>
						</a>
					</li>
					<li title="Selector de capas" class="">
						<a href="#capas" role="tab">
							<i class="fas fa-layer-group">
							</i>
						</a>
					</li>
					<li title="Donaciones">
						<a href="secciones/index.html#4" role="tab" target="_blank">
							<i class="fas fa-hand-holding-usd">
							</i>
						</a>
					</li>
					<li title="Preguntas frecuentes">
						<a href="secciones/index.html#5" role="tab" target="_blank">
							<i class="far fa-question-circle">
							</i>
						</a>
					</li>
					<li title="Registro de cambios">
						<a href="#changelog" role="tab" target="_blank">
							<i class="fas fa-scroll">
							</i>
						</a>
					</li>
					<li title="Contacto">
						<a href="secciones/index.html#6" role="tab" target="_blank">
							<i class="fas fa-envelope-open">
							</i>
						</a>
					</li>
				</ul>
				<ul role="tablist">
					<li title="Configuración del mapa" class="disabled">
						<a href="#settings" role="tab">
							<i class="fas fa-cogs">
							</i>
						</a>
					</li>
				</ul>
			</div>

			<!-- Tab panes -->
			<div class="sidebar-content">
				<div class="sidebar-pane" id="acerca">
					<h1 class="sidebar-header">
						Información general
						<span class="sidebar-close"><i class="fa fa-caret-left"></i></span>
					</h1>
					<div class="elemento-sidebar">
						<div>
							<img src="images/logo.svg" alt="SIG Ferrocarriles Argentinos" class="totalwidth">
						</div>
						<p class="lorem"><span class="advisory">v0.6 - PROYECTO EN DESARROLLO</span></p>
						<p class="lorem"><span class="advisory">© 2014 - 2019, <a rel="me" href="https://twitter.com/@beckermatic">Ariel Sebastián Becker</a></span></p>
						<h3 class="lorem">¿Qué es el <a rel="me" href="https://mastodon.cloud/@ffcc_argentinos">SIG de Ferrocarriles Argentinos?</a></h3>
						<p class="lorem">Es un Sistema de Información Geográfica que permite consultar la información de cualquier elemento de la red de forma sencilla e inmediata.</p>
						<p class="lorem"><b>El proyecto se encuentra en pleno desarrollo; por eso es probable que falten muchas estaciones y ramales, y que algunos ramales presenten curvas sin suavizar. Eso se corregirá en lo sucesivo.</b></p>
						<p class="lorem">Es una herramienta especialmente útil para ingenieros, técnicos y cuadrillas de vía y obras, aunque también es de utilidad para docentes, alumnos, historiadores, escritores, periodistas, legisladores y ciudadanos interesados en el pasado y el presente de nuestra red ferroviaria.</p>
						<p class="lorem">Si bien el proyecto todavía está en una etapa inicial de carga de datos, ya es funcional y se puede consultar la información de estaciones y ramales.</p>
						<p class="lorem">El proyecto se actualiza mensualmente; detrás hay un equipo de cinco personas trabajando a diario para cargar datos y para mejorar la interfaz.</p>
						<p class="lorem"><b>Nuestro proyecto es de acceso libre y gratuito. Para costear los gastos fijos mensuales y anuales es imprescindible contar con aportes y donaciones; <a href="secciones/index.html#page4" target="_blank">aquí explicamos</a> la forma de colaborar con nosotros</b>.</p>
					</div>
				</div>
				<div class="sidebar-pane" id="buscar">
					<h1 class="sidebar-header">Buscador de estaciones<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
					<div class="elemento-sidebar">
						<form class="form-inline">
							<div class="form-group totalwidth">
								<div class="input-group">
									<span class="input-group-addon">
										<i class="fas fa-search"></i>
									</span>
									<input type="text" placeholder="Nombre de la estación" id="buscador" class="form-control">
								</div>
							</div>
						</form>
					</div>
				</div>
				<div class="sidebar-pane" id="leyendas">
					<h1 class="sidebar-header">Leyendas y símbolos<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
					<div class="elemento-sidebar">
						<h3 class="lorem">Ramales</h3>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#FFFFFF;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#00801D;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Operativo
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#FFFFFF;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#FFDD00;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Precaución
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#FFFFFF;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#FF5500;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Clausurado
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#808080;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#FF0000;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Abandonado
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#E3E319;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#FF0000;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Intransitable
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#000000;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#FF0000;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Levantado
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#000000;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#1920E3;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Proyectado
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#808080;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#E37190;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Trazado dudoso
						</p>
						<p class="lorem">
							<svg height="18px" width="18px">
								<line x1="0" y1="0" x2="18" y2="18" style="stroke:#808080;stroke-width:4"></line>
								<line x1="6" y1="6" x2="12" y2="12" style="stroke:#A5A5A5;stroke-width:4;stroke-dasharray=" 2,2"=""></line>
							</svg>
							Sin datos aun
						</p>
						<h3 class="lorem">Estaciones</h3>
						<p class="lorem">
							<svg height="16px" width="16px">
								<circle cx="8" cy="8" r="5" stroke="#FFFFFF" stroke-width="2" fill="#00C800" />
							</svg>
							Activa
						</p>
						<p class="lorem">
							<svg height="16px" width="16px">
								<circle cx="8" cy="8" r="5" stroke="#000000" stroke-width="2" fill="#C8C800" />
							</svg>
							Sin personal
						</p>
						<p class="lorem">
							<svg height="16px" width="16px">
								<circle cx="8" cy="8" r="5" stroke="#000000" stroke-width="2" fill="#D66300" />
							</svg>
							Clausurada
						</p>
						<p class="lorem">
							<svg height="16px" width="16px">
								<circle cx="8" cy="8" r="5" stroke="#646464" stroke-width="2" fill="#D66300" />
							</svg>
							Abandonada
						</p>
						<p class="lorem">
							<svg height="16px" width="16px">
								<circle cx="8" cy="8" r="5" stroke="#FFFFFF" stroke-width="2" fill="#C80000" />
							</svg>
							Desafectada
						</p>
						<p class="lorem">
							<svg height="16px" width="16px">
								<circle cx="8" cy="8" r="5" stroke="#C80000" stroke-width="2" fill="#FFFFFF" />
							</svg>
							Demolida
						</p>
						<p class="lorem">
							<svg height="16px" width="16px">
								<circle cx="8" cy="8" r="5" stroke="#101010" stroke-width="2" fill="#808080" />
							</svg>
							Sin relevar
						</p>

					</div>
				</div>
				<div class="sidebar-pane" id="capas">
					<h1 class="sidebar-header">Capas
						<span class="sidebar-close">
							<i class="fa fa-caret-left"></i>
						</span>
					</h1>
					<div class="elemento-sidebar">
						<div id="panelCapas">
							<h3>Capas de base</h3>
							<div>
								<p>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaBing.png" alt="..." name="imagenCapa" id="capaBing">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Bing satelital</h4>
											Capa que muestra fotografías satelitales de la región abarcada por la pantalla.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaOSM.png" alt="..." name="imagenCapa" id="capaOSM">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Calles, OSM</h4>
											Capa cartográfica con las calles, avenidas y otros elementos geográficos.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" name="imagenCapa" id="capaGray" src="images/capaGray.png" alt="...">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Calles, OSM (gris)</h4>
											Una alternativa más discreta para el fondo cartográfico de OpenStreetMaps.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img id="capaTransporte" class="media-object" name="imagenCapa" src="images/capaTransporte.png" alt="...">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Transporte automotor, OSM</h4>
											Indica los medios de transporte no ferroviarios que se pueden encontrar en inmediaciones de cada estación o punto del mapa.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img id="capaDeportes" class="media-object" name="imagenCapa" src="images/capaDeportes.png" alt="...">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Deportes y recreación</h4>
											Permite encontrar fácilmente actividades recreativas en la región abarcada por la pantalla.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img id="capaTopo" class="media-object" name="imagenCapa" src="images/capaTopo.png" alt="...">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Topográfica (ESRI)</h4>
											Esta capa presenta información topográfica. de la región en pantalla. Es ideal para determinar el tipo de suelo y los accidentes geográficos cercanos.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img id="capaWatercolor" class="media-object" name="imagenCapa" src="images/capaWatercolor.png" alt="...">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Artística</h4>
											Fondo de tipo artístico, ideal para crear presentaciones sencillas y originales.
										</div>
									</div>
								</p>
							</div>
							<h3>Capas de análisis socioeconómico</h3>
							<div>
								<p>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaCanteras.png" alt="..." name="imagenCapaTerceros" id="capaMineria">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Canteras en Neuquén</h4>
											Capa poligonal que define zonas de canteras. Pase el cursor sobre cada icono para conocer la actividad y otros parámetros.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaMataderosBovinos.png" alt="..." name="imagenCapaTerceros" id="capaMataderosBovinos">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Mataderos y frigoríficos de bovinos</h4>
											Capa de puntos que define la ubicación de mataderos y frigoríficos para bovinos. Pase el cursor sobre cada icono para conocer la actividad y otros parámetros.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaAcopio.png" alt="..." name="imagenCapaTerceros" id="capaAcopio">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Centros de acopio por departamento</h4>
											Capa poligonal que define la cantidad de centros de acopio de granos por departamento o partido. Pase el cursor sobre cada departamento para conocer su nombre y su capacidad de acopio.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaElevadores.png" alt="..." name="imagenCapaTerceros" id="capaElevadores">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Elevadores de grano</h4>
											Capa de puntos que define la ubicación de los distintos elevadores de granos en el territorio argentino. Pase el cursor sobre cada uno de ellos para conocer su nombre y otras características.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaLechera.png" alt="..." name="imagenCapaTerceros" id="capaLechera">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Cuencas lecheras</h4>
											Capa poligonal que define las distintas cuencas lecheras del territorio nacional. Pase el cursor sobre cada uno de ellos para conocer sus características.
										</div>
									</div>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaApicultura.png" alt="..." name="imagenCapaTerceros" id="capaApicultura">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Apicultura</h4>
											Capa poligonal que define la cantidad de colmenas y apicultores por departamento o partido. Pase el cursor sobre cada uno de ellos para conocer sus características.
										</div>
									</div>
								</p>
							</div>
							<h3>Capas ferroviarias</h3>
							<div>
								<p>
									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaEstaciones.png" alt="..." name="imagenCapaPropia" id="capaEstaciones">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Estaciones</h4>
											Capa de puntos que define la ubicación, el nombre y otros parámetros de cada estación de la red ferroviaria argentina.
											Si la estación posee una entrada en la enciclopedia, al hacer clic en el icono podrá acceder a ella.
										</div>
									</div>

									<div class="media">
										<div class="media-left">
											<a href="#">
												<img class="media-object" src="images/capaRamales.png" alt="..." name="imagenCapaPropia" id="capaRamales">
											</a>
										</div>
										<div class="media-body">
											<h4 class="media-heading">Ramales</h4>
											Capa de líneas que define los ramales de la red ferroviaria argentina. Esta capa contiene sólo la geometría a grandes rasgos. En un futuro se ofrecerá una capa adicional con los detalles de playas, desvíos de cruce, etc.
										</div>
									</div>
								</p>
							</div>
						</div>
					</div>
				</div>
				<div class="sidebar-pane" id="changelog">
					<h1 class="sidebar-header">Registro de cambios<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
					<div class="elemento-sidebar" id="changelogd">
						<h3 class="lorem">v0.6</h3>
						<ul class="lorem">
							<li>Añadimos capas de análisis socioeconómico (por ahora son sólo tres; en una futura actualización añadiremos más). Organizamos mejor las capas en tres categorías: de base, socioeconómicas y ferroviarias.</li>
						</ul>
						<h3 class="lorem">v0.5</h3>
						<ul class="lorem">
							<li>Añadimos la posibilidad de guardar en marcadores la posición y zoom de la vista actual; también implementamos una regla y añadimos algunas capas adicionales de fondo (políticas, topográficas, y artísticas además de la satelital provista por Bing).</li>
						</ul>
						<h3 class="lorem">v0.4</h3>
						<ul class="lorem">
							<li>Implementamos el uso de geojson-vt y grids, con lo cual la performance mejoró notablemente, sobre todo en la búsqueda y los movimientos (pan y zoom).</li>
						</ul>
						<h3 class="lorem">v0.32</h3>
						<ul class="lorem">
							<li>Añadida una mejora estética en los polyline de los ramales.</li>
							<li>Añadido un changelog, o registro de cambios.</li>
						</ul>
					</div>
				</div>
				<div class="sidebar-pane" id="settings">
					<h1 class="sidebar-header">Configuración<span class="sidebar-close"><i class="fa fa-caret-left"></i></span></h1>
					<div class="elemento-sidebar">
					</div>
				</div>
			</div>
		</div>

<!-- Mapa -->
		<div id="map" class="sidebar-map">
		</div>
		<div id="DialogModal" tabindex="-100">
		</div>
		<script type="text/javascript" src="//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-5c56f2219022444b"></script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131668940-2"></script>
		<script>
			window.dataLayer = window.dataLayer || [];
			function gtag(){dataLayer.push(arguments);}
			gtag('js', new Date());
			gtag('config', 'UA-131668940-2');
		</script>
		<script src="script/app.js"></script>
	</body>
</html>